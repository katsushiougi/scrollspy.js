require [
  "utils/c"
  "utils/ScrollSpy"
  ], (
    c
    ScrollSpy
  ) ->

    scrollSpy = ScrollSpy.getInstance()
    scrollSpy.setOffset 100
    scrollSpy.setup [
      "#blk1"
      "#blk2"
      "#blk3"
      "#blk4"
      "#blk5"
    ]

    $gnav = $ "#gnav"
    scrollSpy.on "SCROLL_STATE_CHANGED", (target) =>
      $gnav.find("li").filter (i, el) ->
        return el.id != target
      .find("a").removeClass("on")
      $("#" + target).find("a").addClass("on")

    $gnav.find("a").on "click", (e) ->
      e.preventDefault()
      scrollSpy.forceStop()
      $me = $(@)
      $gnav.find("li").filter (i, el) ->
        return el.id != $me.parent()[0].id
      .find("a").removeClass("on")
      $me.addClass "on"
      $target = $ @.hash
      posY = $target.offset().top - ( $target.height() / 2 )
      window.scrollTo 0, posY

    scrollSpy.start()
