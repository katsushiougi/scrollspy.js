define ->

  ###
  wait関数
  ###
  wait: (time) ->
    $.Deferred (defer) ->
      setTimeout ->
        defer.resolve()
      , time