define () ->

  ###
  イベントモジュール
  ###

  class Event

    ###
    ###
    on: (ev, callback) ->
      @_callbacks = {} unless @_callbacks?
      evs = ev.split ' '
      for name in evs
        @_callbacks[name] or= []
        @_callbacks[name].push(callback)
      return @

    ###
    ###
    once: (ev, callback) ->
      @on ev, ->
        @off(ev, arguments.callee)
        callback.apply(@, arguments)
      return @

    ###
    ###
    emit: (args...) ->
      ev = args.shift()
      list = @_callbacks?[ev]
      return unless list
      for callback in list
        if callback.apply(@, args) is false
          break
      return @

    ###
    ###
    off: (ev, callback) ->
      unless ev
        @_callbacks = {}
        return @

      list = @_callbacks?[ev]
      return this unless list

      unless callback
        delete @_callbacks[ev]
        return @

      for cb, i in list when cb is callback
        list = list.slice()
        list.splice(i, 1)
        @_callbacks[ev] = list
        break

      return @

  ###
  Exports
  ###
  return Event