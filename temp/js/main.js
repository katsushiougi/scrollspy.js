(function() {
  require(["utils/c", "utils/ScrollSpy"], function(c, ScrollSpy) {
    var $gnav, scrollSpy,
      _this = this;
    scrollSpy = ScrollSpy.getInstance();
    scrollSpy.setOffset(100);
    scrollSpy.setup(["#blk1", "#blk2", "#blk3", "#blk4", "#blk5"]);
    $gnav = $("#gnav");
    scrollSpy.on("SCROLL_STATE_CHANGED", function(target) {
      $gnav.find("li").filter(function(i, el) {
        return el.id !== target;
      }).find("a").removeClass("on");
      return $("#" + target).find("a").addClass("on");
    });
    $gnav.find("a").on("click", function(e) {
      var $me, $target, posY;
      e.preventDefault();
      scrollSpy.forceStop();
      $me = $(this);
      $gnav.find("li").filter(function(i, el) {
        return el.id !== $me.parent()[0].id;
      }).find("a").removeClass("on");
      $me.addClass("on");
      $target = $(this.hash);
      posY = $target.offset().top - ($target.height() / 2);
      return window.scrollTo(0, posY);
    });
    return scrollSpy.start();
  });

}).call(this);
