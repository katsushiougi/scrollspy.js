(function() {
  define(function() {
    return {
      /*
      wait関数
      */

      wait: function(time) {
        return $.Deferred(function(defer) {
          return setTimeout(function() {
            return defer.resolve();
          }, time);
        });
      }
    };
  });

}).call(this);
