(function() {
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(["utils/Event"], function(Event) {
    /*
    スクロールが指定の場所にきたらシグナル通知をするクラス
      - シングルトン
    */

    var ScrollHitEvent;
    ScrollHitEvent = (function(_super) {
      var $document, $window, endPosY, getCurrentPosY, getEndPosY, getScrollTop, scenesArr, setTriggerPos, sortScenes, triggerPosY, wayPoint, _instance;

      __extends(ScrollHitEvent, _super);

      /*
      @private
      */


      _instance = null;

      $window = $(window);

      $document = $(document);

      scenesArr = [];

      endPosY = 0;

      wayPoint = "50%";

      triggerPosY = 0;

      getEndPosY = function() {
        return $document.height() - $window.height();
      };

      getScrollTop = function($el) {
        if ($el.length > 0) {
          return $el.offset().top;
        } else {
          return 0;
        }
      };

      getCurrentPosY = function(posY) {
        var st;
        st = $window.scrollTop();
        return posY - triggerPosY < st || (st / endPosY) >= 1;
      };

      sortScenes = function(scenesArr) {
        return scenesArr.sort(function(a, b) {
          return parseInt(a.posY, 10) - parseInt(b.posY, 10);
        });
      };

      setTriggerPos = function() {
        var start, type, wh;
        wh = $window.height();
        start = wayPoint;
        type = $.type(start);
        if (_.isNumber(type)) {
          return triggerPosY = start;
        } else if (_.isString(type)) {
          return triggerPosY = wh * (parseInt(start) / 100);
        } else {
          return null;
        }
      };

      /*
      @private
      スクロール位置の調査
      */


      ScrollHitEvent.prototype._monitorPos = function() {
        var scene, _i, _len, _results;
        _results = [];
        for (_i = 0, _len = scenesArr.length; _i < _len; _i++) {
          scene = scenesArr[_i];
          if (!scene["complete"]) {
            if (getCurrentPosY(scene["posY"])) {
              this.emit(scene["ev"]);
              _results.push(scene["complete"] = true);
            } else {
              _results.push(void 0);
            }
          } else {
            _results.push(void 0);
          }
        }
        return _results;
      };

      /*
      コンストラクタ
      */


      function ScrollHitEvent() {}

      /*
      モニタリングスタート
      */


      ScrollHitEvent.prototype.start = function() {
        var _this = this;
        endPosY = getEndPosY();
        $window.on("resize", function() {
          endPosY = getEndPosY();
          return setTriggerPos();
        });
        $window.on("scroll", _.bind(this._monitorPos, this));
        return this._monitorPos();
      };

      /*
      トリガーポイントの初期位置を設定
      */


      ScrollHitEvent.prototype.setWayPoint = function(posY) {
        return wayPoint = posY;
      };

      /*
      セットアップ
      */


      ScrollHitEvent.prototype.setup = function(targetArr) {
        var i, tname, _i, _len;
        for (i = _i = 0, _len = targetArr.length; _i < _len; i = ++_i) {
          tname = targetArr[i];
          scenesArr.push({
            $scene: $(tname),
            ev: $.trim("HIT:" + tname),
            posY: getScrollTop($(tname)),
            complete: false
          });
        }
        sortScenes(targetArr);
        return setTriggerPos();
      };

      /*
      シングルトン化
      */


      ScrollHitEvent.getInstance = function() {
        return _instance != null ? _instance : _instance = new ScrollHitEvent;
      };

      return ScrollHitEvent;

    })(Event);
    /*
    Exports
    */

    return ScrollHitEvent;
  });

}).call(this);
