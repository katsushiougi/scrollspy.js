// Fri, 29 Nov 2013 12:43:26 GMT
(function() {
(function() {
  define('utils/c',[],function() {
    return {
      /*
      wait関数
      */

      wait: function(time) {
        return $.Deferred(function(defer) {
          return setTimeout(function() {
            return defer.resolve();
          }, time);
        });
      }
    };
  });

}).call(this);

(function() {
  var __slice = [].slice;

  define('utils/Event',[],function() {
    /*
    イベントモジュール
    */

    var Event;
    Event = (function() {
      function Event() {}

      /*
      */


      Event.prototype.on = function(ev, callback) {
        var evs, name, _base, _i, _len;
        if (this._callbacks == null) {
          this._callbacks = {};
        }
        evs = ev.split(' ');
        for (_i = 0, _len = evs.length; _i < _len; _i++) {
          name = evs[_i];
          (_base = this._callbacks)[name] || (_base[name] = []);
          this._callbacks[name].push(callback);
        }
        return this;
      };

      /*
      */


      Event.prototype.once = function(ev, callback) {
        this.on(ev, function() {
          this.off(ev, arguments.callee);
          return callback.apply(this, arguments);
        });
        return this;
      };

      /*
      */


      Event.prototype.emit = function() {
        var args, callback, ev, list, _i, _len, _ref;
        args = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
        ev = args.shift();
        list = (_ref = this._callbacks) != null ? _ref[ev] : void 0;
        if (!list) {
          return;
        }
        for (_i = 0, _len = list.length; _i < _len; _i++) {
          callback = list[_i];
          if (callback.apply(this, args) === false) {
            break;
          }
        }
        return this;
      };

      /*
      */


      Event.prototype.off = function(ev, callback) {
        var cb, i, list, _i, _len, _ref;
        if (!ev) {
          this._callbacks = {};
          return this;
        }
        list = (_ref = this._callbacks) != null ? _ref[ev] : void 0;
        if (!list) {
          return this;
        }
        if (!callback) {
          delete this._callbacks[ev];
          return this;
        }
        for (i = _i = 0, _len = list.length; _i < _len; i = ++_i) {
          cb = list[i];
          if (!(cb === callback)) {
            continue;
          }
          list = list.slice();
          list.splice(i, 1);
          this._callbacks[ev] = list;
          break;
        }
        return this;
      };

      return Event;

    })();
    /*
    Exports
    */

    return Event;
  });

}).call(this);

(function() {
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define('utils/ScrollSpy',["utils/Event"], function(Event) {
    /*
    スクロールスパイ
      - シングルトン
    */

    var ScrollSpy;
    ScrollSpy = (function(_super) {
      var $document, $window, endPosY, getCurrentPosY, getEndPosY, getScrollTop, nowTarget, offset, prevTarget, scenesArr, setTriggerPos, sortScenes, stopFlg, triggerPosY, wayPoint, _instance;

      __extends(ScrollSpy, _super);

      /*
      @private
      */


      _instance = null;

      $window = $(window);

      $document = $(document);

      scenesArr = [];

      endPosY = 0;

      wayPoint = "50%";

      triggerPosY = 0;

      offset = 0;

      nowTarget = null;

      prevTarget = null;

      stopFlg = false;

      getEndPosY = function() {
        return $document.height() - $window.height();
      };

      getScrollTop = function($el) {
        if ($el.length > 0) {
          return $el.offset().top;
        } else {
          return 0;
        }
      };

      getCurrentPosY = function(posY, endY) {
        var st;
        st = $window.scrollTop();
        return (posY - triggerPosY <= st && st <= endY);
      };

      sortScenes = function(scenesArr) {
        return scenesArr.sort(function(a, b) {
          return parseInt(a.posY, 10) - parseInt(b.posY, 10);
        });
      };

      setTriggerPos = function() {
        var start, type, wh;
        wh = $window.height();
        start = wayPoint;
        type = $.type(start);
        if (_.isNumber(type)) {
          return triggerPosY = start;
        } else if (_.isString(type)) {
          return triggerPosY = wh * (parseInt(start) / 100);
        } else {
          return null;
        }
      };

      /*
      @private
      ステートが変わったかモニタリング
      */


      ScrollSpy.prototype._stateChange = function() {
        if ($window.scrollTop() < offset) {
          nowTarget = 0;
        } else if ($window.scrollTop() >= endPosY) {
          nowTarget = scenesArr.length - 1;
        }
        if (prevTarget !== nowTarget) {
          this.emit("SCROLL_STATE_CHANGED", scenesArr[nowTarget]["target"]);
          return prevTarget = nowTarget;
        }
      };

      /*
      @private
      スクロール位置のモニタリング
      */


      ScrollSpy.prototype._monitorPos = function() {
        var i, scene, _i, _len;
        if (!stopFlg) {
          for (i = _i = 0, _len = scenesArr.length; _i < _len; i = ++_i) {
            scene = scenesArr[i];
            if (getCurrentPosY(scene["posY"], scene["endY"])) {
              nowTarget = i;
            }
          }
          this._stateChange();
        }
        return stopFlg = false;
      };

      /*
      コンストラクタ
      */


      function ScrollSpy() {}

      /*
      オフセット値をセット
      */


      ScrollSpy.prototype.setOffset = function(n) {
        return offset = n;
      };

      /*
      モニタリングスタート
      */


      ScrollSpy.prototype.start = function() {
        var _this = this;
        endPosY = getEndPosY();
        $window.on("resize", function() {
          endPosY = getEndPosY();
          return setTriggerPos();
        });
        $window.on("scroll", _.bind(this._monitorPos, this));
        return this._monitorPos();
      };

      /*
      セットアップ
      */


      ScrollSpy.prototype.setup = function(targetArr) {
        var $scene, i, posY, tname, _i, _len;
        for (i = _i = 0, _len = targetArr.length; _i < _len; i = ++_i) {
          tname = targetArr[i];
          posY = getScrollTop($(tname));
          $scene = $(tname);
          scenesArr.push({
            $scene: $scene,
            target: $scene.data("target"),
            posY: posY,
            endY: posY + $scene.height()
          });
        }
        sortScenes(targetArr);
        return setTriggerPos();
      };

      /*
      強制的にモニタを終了させる
      */


      ScrollSpy.prototype.forceStop = function() {
        return stopFlg = true;
      };

      /*
      シングルトン化
      */


      ScrollSpy.getInstance = function() {
        return _instance != null ? _instance : _instance = new ScrollSpy;
      };

      return ScrollSpy;

    })(Event);
    /*
    Exports
    */

    return ScrollSpy;
  });

}).call(this);

(function() {
  require(["utils/c", "utils/ScrollSpy"], function(c, ScrollSpy) {
    var $gnav, scrollSpy,
      _this = this;
    scrollSpy = ScrollSpy.getInstance();
    scrollSpy.setOffset(100);
    scrollSpy.setup(["#blk1", "#blk2", "#blk3", "#blk4", "#blk5"]);
    $gnav = $("#gnav");
    scrollSpy.on("SCROLL_STATE_CHANGED", function(target) {
      $gnav.find("li").filter(function(i, el) {
        return el.id !== target;
      }).find("a").removeClass("on");
      return $("#" + target).find("a").addClass("on");
    });
    $gnav.find("a").on("click", function(e) {
      var $me, $target, posY;
      e.preventDefault();
      scrollSpy.forceStop();
      $me = $(this);
      $gnav.find("li").filter(function(i, el) {
        return el.id !== $me.parent()[0].id;
      }).find("a").removeClass("on");
      $me.addClass("on");
      $target = $(this.hash);
      posY = $target.offset().top - ($target.height() / 2);
      return window.scrollTo(0, posY);
    });
    return scrollSpy.start();
  });

}).call(this);

define("main", function(){});
} ());